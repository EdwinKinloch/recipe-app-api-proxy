# recipe-app-api-proxy

NGINX proxy app for our receipe app

## Usage

### Environment Variables

* `LISTEN_PORT` - Port to listen on (default: `8000`)
* `APP_HOST` - Hostname of the app to forward request to (default: `app`)
* `APP_PORT` - Port of the app to foward request to (default: `9000`)